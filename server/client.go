package server

import "bufio"
import "net"
import "fmt"
import "bitbucket.org/bps2gr5/mud/parser"


type Client struct{
	Name string
	Id uint
	CurrRoomXPos int
	CurrRoomYPos int
	CurrRoomID int
	CurrRoom *Room

	View string
	Log []string
	LogCounter int
	LogSize int

	Input chan string
	Output chan string
	Reader *bufio.Reader
	Writer *bufio.Writer

	Quit bool

	Server *Server
	Parser *parser.Parser

	Inventory []*Item
}

/*
CONSTRUCTOR FOR CLINET
*/
func NewClient(server *Server, connection net.Conn) *Client{
	//INIT READER/WRITER
	curr_writer := bufio.NewWriter(connection)
	curr_reader := bufio.NewReader(connection)

	//INIT NEW_CLIENT
	client := &Client{
		Input : make(chan string),
		Output : make(chan string),
		Reader : curr_reader,
		Writer : curr_writer,
		CurrRoomID : 0,								
		CurrRoomXPos : 0,
		CurrRoomYPos : 0,
		CurrRoom : server.WelcomeScreen, 

		Log : make([]string,0),
		LogCounter : -1,
		LogSize : 5,

		Server : server,
		Parser : parser.New(),

		Quit : false,

		Inventory : []*Item {
			newItem(POTION),
		},
	}
	//INIT LOG
	client.Log = []string{}
	for i := 0; i < client.LogSize; i++ {
		client.Log = append(client.Log, "")
	}
	//ASSIGN CLIENT TO SPAWN ROOM
	server.RoomList[client.CurrRoomYPos][client.CurrRoomXPos].AddClientToList(client)

	//POPULATE PARSER
	client.PopulateParser()

	client.Handle()

	fmt.Println("New Client launched...")

	return client
}
/*
Removes Client from every server internal lists after logout.
*/
func (client *Client) End(){
	//IMPLEMENT ERASE OF CLIENT RESOURCES
	client.Quit = true;
	newClientList := []*Client{}
	//DELETE FROM ROOM CLIENT LIST
	for _,c := range client.CurrRoom.ClientList{
		if(c != client){
			newClientList = append(newClientList, c)
		}
	}
	client.CurrRoom.ClientList = newClientList
	//EMPTY NEW CLIENT LIST
	newClientList = []*Client{}
	//DELETE FROM SERVER CLIENT LIST
	for _,c := range client.Server.ClientList{
		if(c != client){
			newClientList = append(newClientList, c)
		}
	}
	client.Server.ClientList = newClientList

	fmt.Println("Client Ended")
}
/*
Starts go-routines for listening and writing to telnet client
*/
func (client *Client) Handle(){
	go client.WriteToClient()
	go client.ReadFromClient()
}
func (client *Client) WriteToClient(){
	for message := range client.Output{
		client.Writer.WriteString(message)
		client.Writer.Flush()
		fmt.Println("flushed....")
	}
}
func (client *Client) ReadFromClient(){
	for ; client.Quit == false;{
		fmt.Println("Waiting for next Message...")
		message, readerErr := client.Reader.ReadString('\n')
		if readerErr != nil{
			fmt.Println("PROBLEMS WITH READER")
			fmt.Println("CLOSING DOWN READER / ENDING CLIENT")
			client.End()
		}
		err := client.Parser.Execute(message)
		if err != nil {
			client.SetView(err.Error())
		}
	}
}

//PARSER FUNCS
/*
Loads basic logIn and signIn functions onto parser.
*/
func (client *Client) PopulateParser(){
	client.Parser.AddCommand("login", "Use to log in", 2, client.login)
	client.Parser.AddCommand("signup", "Use to signup", 2, client.signup)

}
/*
Loads game functions onto parser.
*/
func (client *Client) InflateParser(){
	client.Parser.RemoveCommand("login")
	client.Parser.RemoveCommand("signup")
	client.Parser.AddCommand("talk", "Use to talk", 1, client.Talk)
	client.Parser.AddCommand("walk", "Use to walk", 1, client.SwitchRoom)
	client.Parser.AddCommand("use", "Use an item", 1, client.UseItem)
	client.Parser.AddCommand("inventory", "", 0, client.ShowInventory)
	client.Parser.AddCommand("search", "", 0, client.SearchForItems)
	client.Parser.AddCommand("pickup", "", 1, client.PickUpItem)
	client.Parser.AddCommand("drop", "", 1, client.DropItem)
	client.Parser.AddCommand("give", "<item> <player>", 2, client.HandItemToPlayer)
	client.Parser.AddCommand("lookaround", "", 0, client.LookAround)

}

//CLIENT USER FUNCTIONS
func (client *Client) login(args []string){
	err := client.Server.PlayerManager.Login(args[0],args[1])
	if err != nil{
		client.SetView(err.Error())
		return
	}
	client.Name = args[0]
	client.CurrRoom = client.Server.RoomList[0][0]
	client.LookAround([]string{})
	client.Server.GlobalBroadcast(client.Name + " is online.")
	client.InflateParser()
}

func (client *Client) signup(args []string) {
	err := client.Server.PlayerManager.AddPlayer(args[0], args[1], false)
	if err != nil {
		client.SetView(err.Error())
		return
	}
	client.login(args)
}

func (client *Client) SwitchRoom(direction []string){
	fmt.Println(direction[0])
	switch {
	case direction[0] == "north":
		fmt.Println("north")
		client.walk(0,1)
	case direction[0] == "south":
		fmt.Println("south")
		client.walk(0,-1)
	case direction[0] == "east" :
		fmt.Println("east")
		client.walk(1,0)
	case direction[0] == "west" :
		fmt.Println("west")
		client.walk(-1,0)
	default:
		client.SetView("Unknown direction. Walking in circles.")
	}
}
func (client *Client) walk(x int, y int){
	if(client.CurrRoomYPos+y <= client.Server.MapSize -1 && client.CurrRoomXPos+x <= client.Server.MapSize -1){
		if(client.CurrRoomYPos +y >= 0 && client.CurrRoomXPos +x >= 0 ){
			//LOCK SEMAPHOR
			client.Server.Semaphor <- 1
			//FETCH ROOMS
			fmt.Println("ERLAUBT")
			currRoom := client.Server.RoomList[client.CurrRoomYPos][client.CurrRoomXPos]
			nextRoom := client.Server.RoomList[client.CurrRoomYPos + y][client.CurrRoomXPos + x]
			//DELET CLIENT FROM CURRENT ROOM
			client.CurrRoom.DelClientFromList(client)
			fmt.Println("Room size after del: ", len(client.CurrRoom.ClientList))
			message := fmt.Sprintf("%s", client.Name + " left the " + currRoom.Name)
			client.broadcast(message)
			//MOVE CLIENT + ASSIGN ROOMID
			client.CurrRoomYPos += y
			client.CurrRoomXPos += x
			client.CurrRoom = nextRoom
			//SET VIEW
			//ADD CLIENT TO NEW ROOM
			nextRoom.AddClientToList(client)
			message = fmt.Sprintf("%s", client.Name + " entered the " + client.CurrRoom.Name)
			client.broadcast(message)
			client.LookAround([]string{})
			//RELEAE SEMAPHOR
			<-client.Server.Semaphor
			return
		}

	}
	client.SetView("You shall not pass !")
}

func (client *Client) Talk(args []string) {
	substring := ""
	for x := range args {
		substring += args[x] + " "
	}
	message := fmt.Sprintf("%s", client.Name + ": " + substring)
	client.broadcast( message)
}

func (client *Client) broadcast(msg string) {
	client.Input <- msg
	<- client.Server.BroadcastFinished
}


//CLIENT UTILS
func moveCursorToEnd() string {
	return fmt.Sprintf("%c[35;0H",27)
}
func format(message string) string {
	return fmt.Sprintf("%s%cE", message, 27)
}
func ClearScreen() string{
	return fmt.Sprintf("%c[2J%c[0;0H", 27, 27)
}
func (client *Client) UpdateScreen(){
	update := ClearScreen()
	//Print Current Room Name
	update += format(client.CurrRoom.Name)
	//MARGIN
	update += format("") + format("")
	update += format(client.View)
	//MARGIN
	update += format("") + format("")
	update += format("") + format("")

	//Print Room Messages
	update += client.ReadLog()
	//Print Console

	update += moveCursorToEnd()
	update += ">>> "

	client.Output <- update
}

func (client *Client) LookAround([]string) {
	var player string
	if len(client.CurrRoom.ClientList) == 1 {
		player = "You are alone."
	} else {
		player = "You see the following people: "
		for _, pl := range client.CurrRoom.ClientList {
			if pl != client {
				player += " " + pl.Name
			}
		}
	}
	client.SetView(format(client.CurrRoom.Description) + format(player))
}



//LOG && VIEW
func (client *Client) AddLogEntry(message string){
	client.LogCounter = (client.LogCounter + 1) % client.LogSize
	client.Log[client.LogCounter] = "> " + message

	client.UpdateScreen()
}
func (client *Client) ReadLog() string {
	message :=  ""
	for i:= 0; i < client.LogSize; i++{
		message += format(client.Log[(client.LogCounter+1+i)%client.LogSize])
	}
	return message
}

func (client *Client) SetView(event string){
	client.View = event

	client.UpdateScreen()
}
