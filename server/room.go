package server

import "os"
import "io"
import "log"
import "encoding/json"
import "math/rand"
import "fmt"

type Room struct{
	XPos int
	YPos int
	RoomID int
	Name string
	Description string
	ClientList []*Client
	Items []*Item
}

type field struct{
	Name string
	Description string
}

func LoadRoomFields(roomFieldsFile string) []field{
	roomFieldsData, err := os.Open(roomFieldsFile)

	defer roomFieldsData.Close()
	if err != nil {
		log.Fatal("Unable to open file: ", err.Error())
	}
	jsonParser := json.NewDecoder(roomFieldsData)
	fieldsContainer := []field{}
	for {
		currField := field{}
		if err = jsonParser.Decode(&currField); err == io.EOF{
			break
		} else if err != nil{
			log.Fatal("Parsing roomFields: ", err.Error())
		}
		fieldsContainer = append(fieldsContainer, currField)
	}
	return fieldsContainer
}

func PopulateRoomMap(mapSize int) [][]*Room{
	fieldsContainer := LoadRoomFields("roomFields.json")
	roomMap := [][]*Room{}
	roomID := 0
	for i:=0;i<mapSize;i++{
		xSlice := []*Room{}
		for j:=0;j<mapSize;j++{
			currRoom := &Room{
				XPos : j,
				YPos : i,
				RoomID : roomID,
				Name : fieldsContainer[roomID].Name,
				Description : fieldsContainer[roomID].Description,
				ClientList : make([]*Client,0),
			}
			currRoom.SpawnItems()
			roomID++
			xSlice = append(xSlice, currRoom)
		}
		roomMap = append(roomMap , xSlice)
	}
	return roomMap
}

func (room *Room) DelClientFromList(client *Client){
	newClientList := []*Client{}
	for _,x := range room.ClientList{
		if(x != client){
			newClientList = append(newClientList, x)
		}
	}
	room.ClientList = newClientList
}
func (room *Room) AddClientToList(client *Client){
	room.ClientList = append(room.ClientList, client)
	fmt.Println("ADDED CLIENT TO ROOM...")
	fmt.Println(len(room.ClientList))
}

func (room *Room) SpawnItems() {
	for i:=0;i<len(itemNames);i++{
		if(rand.Float32()>0.7){
			room.Items = append(room.Items, newItem(i))
		}
	}
}

func (room *Room) RemoveItem(name string) {
	newItems := []*Item{}
	found := false
	for _, item := range room.Items {
		if !found && item.Name == name {
			found = true
		} else {
			newItems = append(newItems, item)
		}
	}
	room.Items = newItems
}
