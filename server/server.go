package server

import "fmt"
import "net"
import "bitbucket.org/bps2gr5/mud/plmanager"


var TitleScreen = "Welcome!"


type Server struct{
	ClientList []*Client
	PlayerManager *plmngr.Playermanager
	MapSize int
	RoomList [][]*Room
	joining chan net.Conn
	incoming chan string
	Semaphor chan int
	BroadcastFinished chan int
	WelcomeScreen *Room
}

/*
Func StartServer initiates the server. 
It now handles incoming connection and server traffic.
@see StartServerHandler
*/
func StartServer(PORT string){
	//LAUNCHING SERVER
	//POPULATING ROOMS(MAP)
	mapSize := 4
	roomList := PopulateRoomMap(mapSize)

	//INIT SERVER
	thisServer := &Server{
		ClientList : make([]*Client,0),
		PlayerManager : plmngr.New("logins.json"),
		MapSize : mapSize,
		RoomList : roomList,
		joining : make(chan net.Conn),
		incoming : make(chan string),
		Semaphor : make(chan int,1),
		BroadcastFinished : make(chan int),
	}
	thisServer.WelcomeScreen = &Room{
		Name: "",
		Description: TitleScreen,
	}

	go StartServerHandler(thisServer)

	//INIT SERVER CONNECTION
	listener,_ := net.Listen("tcp",PORT)
	fmt.Println("Server is listening...")

	for {
		connection,_ := listener.Accept()
		fmt.Println("Connection incoming!")
		thisServer.joining <- connection
	}
}
/*
Once this method is called the server is able to handle incomming connection.
@see JoinServer
*/
func StartServerHandler(thisServer *Server){
	for{
		select{
		case x := <-thisServer.joining:
			JoinServer(thisServer, x)
		/*case x := <-thisServer.incoming:
			fmt.Println(x)*/
		}
	}
}
/*
JoinServer is called after the server detected an incoming connection it creates a new Client entity
by calling the NewClient method. Also it appends the new Client to the Server internal ClientList
*/
func JoinServer(thisServer *Server, clientConn net.Conn){
	currClient := NewClient(thisServer, clientConn)
	thisServer.ClientList = append(thisServer.ClientList, currClient)

	go func(){
		for{
			msg := <-currClient.Input
			thisServer.Broadcast(currClient.CurrRoom ,msg)
			thisServer.BroadcastFinished <- 1
		}
	}()

	currClient.SetView(TitleScreen)
}


/*
The Broadcast function is called every time a Client is sending a message inside
his current room.
Broadcast adds this message to every client that is currently listed in the room.
*/
func (thisServer *Server) Broadcast(room *Room, message string){
	fmt.Println("Server is Broadcasting...")
	fmt.Println("Room Size during bc: ", len(room.ClientList))
	fmt.Println("BCing in Room ", room.Name)
	for _,client := range room.ClientList{
		client.AddLogEntry(message)
	}
}
/*
GlobalBroadcast adds a given message to every clients log connected to the server.
*/
func (thisServer *Server) GlobalBroadcast(message string) {
	for _, client := range thisServer.ClientList {
		if client.CurrRoom != thisServer.WelcomeScreen {
			client.AddLogEntry(message)
		}
	}
}
