package server

import "fmt"


//type itemId int

const (
    POTION int = iota
    HORN
    MAGICWAND
    EGG
)

var itemNames map[int] string

func init() {
    itemNames = map[int] string {
        POTION : "SuperPotion",
        HORN : "MightyHorn",
        MAGICWAND : "MagicWand",
        EGG : "Egg",
    }
}

type Item struct {
    Id int
    Name string
}


func newItem(iid int) *Item {
    item := &Item {
        Id: iid,
        Name: itemNames[iid],
    }
    return item
}

//ITEMS
func (client *Client) UseItem(args []string) {
	itemName := args[0]
	itemInd := -1
	for i, item := range client.Inventory {
		if item.Name == itemName {
			itemInd = i
		}
	}
	if itemInd == -1 {
		client.SetView("No such item in your inventory")
		return
	}
	item := client.Inventory[itemInd]

	switch item.Id {
	case POTION :
		client.broadcast(fmt.Sprintf("%s drank a %s. Nothing happened.", client.Name, item.Name))
	case HORN :
		msg := fmt.Sprintf("%s blows the %s. The sound is heard all over the world.", client.Name, item.Name)
		client.Server.GlobalBroadcast(msg)
	case MAGICWAND :
		msg := fmt.Sprintf("%s uses the %s. A pumpkin appears.", client.Name, item.Name)
		client.broadcast(msg)
	case EGG :
		msg := fmt.Sprintf("%s looks at the %s and wonders if it existed before the chicken.", client.Name, item.Name)
		client.broadcast(msg)
	}
	//DELETES ITEM AFTER USAGE
	newInventory := []*Item{}
	for i, cItem := range client.Inventory {
		if i != itemInd {
			newInventory = append(newInventory, cItem)
		}
	}
	client.Inventory = newInventory
}

func (client *Client) ShowInventory(args []string) {
	msg := format("Your Inventory: ")
	for _,item := range client.Inventory {
		msg += format(item.Name)
	}
	client.SetView(msg)
}

func (client *Client) AddItem(item *Item) {
	client.Inventory = append(client.Inventory, item)
}

func (client *Client) RemoveItem(name string) *Item {
	newInventory := []*Item{}
	var remItem *Item = nil
	for _, cItem := range client.Inventory {
		if remItem == nil && cItem.Name == name {
			remItem = cItem
		} else {
			newInventory = append(newInventory, cItem)
		}
	}
	client.Inventory = newInventory
	return remItem
}

func (client *Client) SearchForItems(args []string) {
	msg := format("You found these items laying on the ground:")
	for _, item := range client.CurrRoom.Items {
		msg += format(item.Name)
	}
	client.SetView(msg)
}

func (client *Client) PickUpItem(args []string) {
	itemName := args[0]
	for _, item := range client.CurrRoom.Items {
		if item.Name == itemName {
			client.Inventory = append(client.Inventory, item)
			client.CurrRoom.RemoveItem(item.Name)
			client.broadcast(client.Name + " picked up " + item.Name)
			return
		}
	}
	client.SetView("No such item.")
}

func (client *Client) DropItem(args []string) {
	itemName := args[0]
	newInventory := []*Item{}
	found := false
	for _, cItem := range client.Inventory {
		if !found && cItem.Name == itemName {
			client.CurrRoom.Items = append(client.CurrRoom.Items, cItem)
			found = true
		} else {
			newInventory = append(newInventory, cItem)
		}
	}
	client.Inventory = newInventory
	if found {
		client.broadcast(client.Name + " dropped " + itemName + ".")
	} else {
		client.SetView("No such item in your inventory.")
	}
}

func (client *Client) HandItemToPlayer(args []string) {
	itName := args[0]
	plName := args[1]
	var receiver *Client = nil
	for _, player := range client.CurrRoom.ClientList {
		if player.Name == plName {
			receiver = player
		}
	}
	if receiver == nil {
		client.SetView("No such player in this room.")
		return
	}
	item := client.RemoveItem(itName)
	if item == nil {
		client.SetView("No such item.")
		return
	}
	receiver.AddItem(item)
	client.broadcast(client.Name + " gave item " + item.Name + " to " + receiver.Name)
}
