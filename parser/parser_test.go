package parser

import "testing"


type point struct {
	x string
	y string
}

func (v *point) set(xy []string) {
	v.x, v.y = xy[0], xy[1]
}

func TestParser(t *testing.T) {
	i := point{}
	p := New()
	p.AddCommand("xy", "", 2, i.set)
	err := p.Execute("xy 5 4")
	if err != nil {
		t.Error(err)
	} else if i.x != "5" || i.y != "4" {
		t.Error(i)
	}
	err = p.Execute("echo test")
	if err == nil {
		t.Error("Excpected an error")
	}
	err = p.Execute("xy 5")
	if err == nil {
		t.Error("Excpected another error")
	}
	err = p.Execute("xy")
	if err == nil {
		t.Error("Excpected yet another error")
	}
	p.RemoveCommand("xy")
	if p.Execute("xy 4 2") == nil {
		t.Error("Removing commands")
	}
}
