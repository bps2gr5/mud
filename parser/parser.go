package parser

import "errors"
import "strings"


type Action func([]string)

type Parser struct {
	cmds map[string]command
}

type command struct {
	name string
	description string
	minArgNum int
	action Action
}

/*
Create a new Parser
*/
func New() *Parser {
	return &Parser { cmds: map[string]command{} }
}


/*
Add a new command to the parser
*/
func (p Parser) AddCommand(name, description string, minArgNum int, a Action) {
	p.cmds[name] = command {
		name,
		description,
		minArgNum,
		a,
	}
}


/*
Remove a command from the parser
*/
func (p *Parser) RemoveCommand(name string) {
	newCmds := map[string]command{}
	for nm, cmd := range p.cmds {
		if nm != name {
			newCmds[nm] = cmd
		}
	}
	p.cmds = newCmds
}

/*
Tokenize input and execute the corresponding command
*/
func (p Parser) Execute(input string) error {
    input = strings.TrimFunc(input, isCtrlChr)
	tokens := strings.Split(input, " ")
	name := tokens[0]
	args := tokens[1:]
	cmd, ok := p.cmds[name]
	if !ok {
		return errors.New("Unknown command")
	}
	if len(args) < cmd.minArgNum {
		return errors.New("Too few arguments")
	}
	//Execute command
	cmd.action(args)
	return nil
}

func isCtrlChr(r rune) bool {
    return r < 33 || r > 127
}
