package plmngr

import "os"
import "io"
import "log"
import "errors"
import "bufio"
import "encoding/json"
import "crypto/md5"


type md5Hash [md5.Size]byte

type login struct {
	User string
	Pass []byte
}

type player struct {
	pw md5Hash
	online bool
}

type Playermanager struct {
	logins map[string]*player
	loginFile string
}

/*
Create a new Playermanager and load the login data
from loginFile
*/

func New(loginFile string) *Playermanager {
	loginData, err := os.Open(loginFile)
	defer loginData.Close()
	check("Opening file", err)
	jsonParser := json.NewDecoder(loginData)
	logins := map[string]*player{}
	for {
		l := login{}
		err := jsonParser.Decode(&l)
		if err == io.EOF {
	        break
	    }
	    check("Parsing login-file", err)
	    logins[l.User] = &player{toMD5(l.Pass), false}
	}
    return &Playermanager{logins, loginFile}
}

/**
check an error
*/
func check(msg string, err error) {
	if err != nil {
		log.Fatal(msg, ": ", err.Error())
	}
}

/*
returns md5-Hash of a string
*/
func getMD5(input string) md5Hash {
	return md5.Sum([]byte(input))
}

/*
convert a byte-slice into a md5-Hash
*/
func toMD5(input []byte) md5Hash {
	m := [md5.Size]byte{}
	copy(m[:], input[0:md5.Size])
	return md5Hash(m)
}

/*
update the loginfile
*/
func (plm Playermanager) writePlayersToFile() {
	//Create an empty file
	f, err := os.Create(plm.loginFile)
	check("updating login file", err)
	defer f.Close()
	//write all players into the file
	loginData := bufio.NewWriter(f)
	for user, pl := range plm.logins {
		l := login{user, pl.pw[:md5.Size]}
		encl, err := json.Marshal(l)
		check("Encoding logindata", err)
		loginData.Write(encl)
	}
	loginData.Flush()
}

/*
Create a new player
*/
func (plm Playermanager) AddPlayer(user, pass string, tmp bool) error {
	_, ok := plm.logins[user]
	if ok {
		return errors.New("Username already in use")
	}
	plm.logins[user] = &player{getMD5(pass), false}
	if !tmp {
		plm.writePlayersToFile()
	}
	return nil
}

/*

*/
func (plm Playermanager) Login(user, pass string) error {
	pHash := getMD5(pass)
	pl, ok := plm.logins[user]
	if !ok || pl.pw != pHash {
		return errors.New("Wrong name or password")
	} else if pl.online {
		return errors.New("Player already online")
	}
	pl.online = true
	return nil
}

func (plm Playermanager) Logout(user string) {
	pl, ok := plm.logins[user]
	if ok {
		pl.online = false
	}
}
