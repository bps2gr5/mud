package plmngr

import "testing"


func TestLogin(t *testing.T) {
	plm := New("logins.json")
	// plm.AddPlayer("peter", "test", false)
	// plm.AddPlayer("horst", "test", false)
	if plm.AddPlayer("horst", "test", true) == nil {
		t.Fatal("Name registered twice")
	}
	err := plm.Login("peter", "test")
	if err != nil {
		t.Fatal("Login: ", err)
	} else if plm.Login("gustav", "test") == nil {
		t.Fatal("Login with wrong name")
	} else if plm.Login("horst", "wrongpass") == nil {
		t.Fatal("Login with wrong pw")
	} else if plm.Login("peter", "test") == nil {
		t.Fatal("Double login")
	}
}