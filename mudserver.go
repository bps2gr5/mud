package main

import "fmt"
import "io/ioutil"
import "bitbucket.org/bps2gr5/mud/server"

const PORT = ":2222"


func main() {
	title, err := ioutil.ReadFile("server/title.txt")
	if (err != nil) {
		fmt.Println("loading title: ", err.Error())
	} else {
		server.TitleScreen = string(title)
	}
	server.StartServer(PORT)
}
